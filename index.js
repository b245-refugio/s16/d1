// console.log("Hello World");

// [SECTION] Arithmetoc Operators
	//  allow us to perform mathematical operations between operands (value on either sides of the operator).
	// it returns a numerical value.

	let x = 101;
	let y = 25;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of difference operator: " + difference);

	let product = x * y;
	console.log("Result of product operator: " + product);

	let quotient = x / y;
	console.log("Result of quotient operator: " + quotient);

	let remainder = x % y;
	console.log("Result of modulo operator: " + remainder);

	// Assignment Operator
		// assigns the value of the right operand to a "variable".
	// Basic Assignment Operator
	let assignementNumber = 8;
	console.log(assignementNumber);

	// Addition Assignment Operator (+=)
	// long method
	// assignementNumber = assignementNumber + 2;
	
	// shorthand method
	assignementNumber += 2;
	console.log("Result of addition assignment operator: "+assignementNumber);

	// Subtraction/Multiplication/Divisioin Assignment Operator (-=, *=, /=)

	assignementNumber -= 2;
	console.log("Result of subtration assignment operator: "+assignementNumber);

	assignementNumber *= 2;
	console.log("Result of multiplication assignment operator: "+assignementNumber);

	assignementNumber /= 2;
	console.log("Result of division assignment operator: "+assignementNumber);

	assignementNumber %= 2;
	console.log("Result of modulo assignment operator: "+assignementNumber);

	// PEMDAS (Order of Operations)
	
	// Multiple Operators and Parenthesis

	/*
		1. 3*4 = 12
		2. 12/5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6 // actual

	*/
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS operation: " + mdas);

	// The order of operations can be changed by adding a  parenthesis

	let pemdas = 1 + (2-3) * (4/5);
	console.log("Result of PEMDAS operation: " + pemdas);

	pemdas = (1 + (2-3)) * (4/5);
	console.log("Result of PEMDAS operation: " + pemdas);

// [SECTION] Increment and Decrement
	// This operators add or subtract values by 1 and reassign the value of the variables where the increment/decrement was applied.

	let z = 1;

	// Increment (++)
	// The value of "z" is added by one before storing it in the "increment" variable.
	let increment = ++z;
	//pre-increment both  and  z variable have value of "2"
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment for z: " +z);

	// The value of "z" is stored in the "increment" variable before it is incresed by one.
	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment for z: " + z);

	// Decrement(--)
	z = 5
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log ("Result of pre-decrement for z: " + z);

	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log ("Result of post-decrement for z: " + z);

// [SECTION] Type Coercion
	// automatic or implicit conversion of value from one data type to another.
	// This happens when operation are performed on different data types that would normally not be possible and yield irregular results.
	// "automatic conversion"

	let numA = '10';
	let numB = 12;
	// Performing addition operation to a number and string variable will result to concatenation.
	let coercion = numB + numA;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	/*

		- The boolean true is also assciated with the value of 1.
		- The boolean false is also associated with the value of 0.

	*/


	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

// [SECTION] Comparison Operation
	// are used to evaluate and compare the left and right operands.
	//  it return a Boolean value.

	let juan = 'juan';


	// Equality Operator (==)
	/*

		- Checks whether the operands are equal/have the same content.
		- Attemps to CONVERT AND COMPARE operands of different data tyypes (type coercion)
	*/
	console.log("Equality Operator");
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(false == 0); //true
	console.log('juan' == "juan"); //true
	console.log("juan" == "Juan"); //false
	console.log(juan == 'juan'); //true

	// Inequality Operator (!=)
	/*
		- Checks whether the operands are not equal/ have different data types.
		- Attempts tp CONVERT AND COMPARE operands of different data types.
	*/
	console.log("Inequality Operator");
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(false != 0); //false
	console.log('juan' != "juan"); //false
	console.log("juan" != "Juan"); //true
	console.log(juan != 'juan'); //false

	// Strictly Equality Operator (===)
	/*
		- check whether the operands are equal/have the same content.
		- also COMPARES the data type of 2 values.

	*/

	console.log("Strict Equality Operator:")
	console.log(1===1); //true
	console.log(1===2);	//false
	console.log(1==='1') //false
	console.log(false===0); //false
	console.log('juan'==="juan"); //true
	console.log("juan"==="Juan"); //false
	console.log(juan==='juan'); //true

	// Strict Inequality Operator (!==)
	/*
		- checks where the operands are not equal/ have the different content.
		- also compares the data type of 2 values.

	*/

	console.log("Strict Inequality Operator:")
	console.log(1!==1); //false
	console.log(1!==2);	//true
	console.log(1!=='1') //true
	console.log(false!==0); //true
	console.log('juan'!=="juan"); //false
	console.log("juan"!=="Juan"); //true
	console.log(juan!=='juan'); //false

// [SECTION] Greater than and Less than Operator
	// Some comparison operatos check whether one value is greater or less than to the other value.
	// returns a boolean value.


	let a = 50;
	let b = 65;

	console.log("Greater than and less than Operator")
	// GT or Greater than (>)
	let isGreaterThan = a > b; //false
	console.log(isGreaterThan);

	// GT or Greater than (<)
	let isLessThan = a < b; //true
	console.log(isLessThan);

	// GTE or Greater Than or Equal (>=)
	// b=50; changing the vale of b to 50 will result to true.
	let isGTorEQual = a >= b; //false
	console.log(isGTorEQual);

	// LTE or Less Than or equal (<=)
	let isLTorEqual = a<=b; //true
	console.log(isLTorEqual);

	let numStr = "30";
	console.log(a > numStr); //true - forced coercion to change string to a number

	let strNum = "twenty";
	console.log(b > strNum); // what ever operator is used it will result to false, since the string is not numeric. This is usually result to NaN (Not a Number)

// [SECTION] Logical Operators
	// to allow for a more specific logical combination of conditions and evaluations.
	// it returns a Boolean value.

	let isLegalAge	= true;
	let isRegistered = false;

	// Logical AND Operator (&& - Double Ampersand)
	// Returns TRUE if all operands are true.
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " + allRequirementsMet);


	// Logical OR Operator (|| - Double Pipe)
	// Returns TRUE of one of the operands are TRUE
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " + someRequirementsMet);

	// Logical Not Operator (! - Exclamation Point)
	// Returns the opposite value.
	console.log("Result of logical NOT Operator: " + !isRegistered);
	console.log("Result of logical NOT Operator: " + !isLegalAge);